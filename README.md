# FEL SMP - 2024 - Lekari



## úvodní informace

Výtejte na stránkách týmu Lékaři pro předmět SMP - Úterý 8:15 - letní semestr 2024

Studenti pracující na projektu:

- krepeja1 - Jan Viktor Křepelka
- kadlcjar - Jaromír Kadlčík
- nguyem15 - Michal Nguyen
- kantosam - Samuel Jan Kantor

Na těchto GitLab stránkách naleznete všechny potřebné informace o projektu. Navštivte [Wiki](https://gitlab.fel.cvut.cz/krepeja1/fel-smp-2024-lekari/-/wikis/home) stránky pro více informací.
